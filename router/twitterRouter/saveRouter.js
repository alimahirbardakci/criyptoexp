const express = require('express');
const Router = express.Router();

const db = require('../../db')
const mail = require('../../sendMail');

const LastTweet = require('../../models').LastTweet;
const CryptoModel = require('../../models').CryptoModel;


Router.get('/last-tweets/all', (req,res) => {
  LastTweet.find({}, (err,docs) => {
    res.send(docs)
  })
});

Router.get('/last-tweets/:username', (req,res) => {
  const user = req.params.username;
  const queires = req.query.word.split(',');
  console.log(queires);
  let regexStr='';
  queires.forEach((el) => {
    regexStr += el + '|';
  });
  regexStr = regexStr.slice(0,-1);
  let selectedTweets = [];
  let regex = new RegExp("\\b(" + regexStr + ")\\b", "gi");
  console.log(regex);
  // let regex = new RegExp(req.query.word, 'gi')

  LastTweet.find({}, (err, docs) => {
    Object.values(docs[0].data[user]).forEach((el) => {
      found = el.text.match(regex);
      if(found) {
        selectedTweets.push(el);
      };
    })
  }).then(() => {
    if(selectedTweets.length > 0) {
      res.send(selectedTweets);
      message='';
      selectedTweets.forEach((el,index) => {
        message += (index + 1)+ ' -)' + el.text + '\n\n';
      });
      console.log(message);
      // mail(message);
    } else {
      res.send('not match')
    }

  });
})

Router.get('/users/:username', (req,res) => {
  let arr = [];

  CryptoModel.find({}, (err,docs) => {
    docs.forEach((item) => {
      arr.push(item.data.filter((el) => el.username.toLowerCase() == req.params.username )[0]);
    });
    return arr;
  }).then((result) => {
    res.send(result)
  })
});

module.exports = Router

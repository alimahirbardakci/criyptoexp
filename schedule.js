const acount = require('./acounts');
const cron = require('node-cron');
const axios = require('axios');
const sendMail = require('./sendMail')
const LastTweet = require('./models').LastTweet;
const CryptoModel = require('./models').CryptoModel;

const bearerToken = ''
const endpointURL = "https://api.twitter.com/2/users/by?"
const params = 'user.fields=created_at,description,entities,id,location,name,pinned_tweet_id,profile_image_url,protected,public_metrics,url,username,verified,withheld&';

let searchAcount = 'usernames=';
acount.forEach((el) => {
  searchAcount += el.twitter;
});

function schedule() {
  cron.schedule('*/30 * * * *', () => {

    //GET TWEETS AND SAVE
    const requests = [];
    const testArr = {}
    acount.forEach((el) => {
      requests.push(axios.get(`https://api.twitter.com/2/users/${el.twitterId}/tweets`,{
        headers: {
          "User-Agent": "v2UserLookupJS",
          "authorization": `Bearer ${bearerToken}`
        }
      }));
    });
    axios.all(requests).then(axios.spread((...responses) => {
      for (let i = 0; i < responses.length; i++) {
        testArr[acount[i].coinMarket] = {...responses[i].data.data}
      }
    })).then(() => {
      LastTweet.find({}, (err, docs) => {
      }).then((res) => {
        if (res) {
          for(let i = 0; Object.keys(res[0].data).length > i; i++) {
            if(Object.values(res[0].data)[i][0].id == Object.values(testArr)[i][0].id) {
              return;
            } else {
              LastTweet.findOneAndUpdate({ _id : res[0]._id}, {data: testArr}, () => {
              });
            };
          };
        } else {
          console.log('HATAAAAAAAAAA');
        }
      });
    });

    //GET USER INFO,COIN INFO and SAVE
    const req1 = axios.get(`${endpointURL}${params}${searchAcount}`,{
      headers: {
        "User-Agent": "v2UserLookupJS",
        "authorization": `Bearer ${bearerToken}`
      }
    });
    const req2 = axios.get('https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest',{
      headers: {
        "X-CMC_PRO_API_KEY": "271bc80a-b03b-4843-845f-c8fb45a89f32",
      }
    });
    const req3 = axios.get(`https://api.twitter.com/2/users/2590633042/tweets`,{
      headers: {
        "User-Agent": "v2UserLookupJS",
        "authorization": `Bearer ${bearerToken}`
      }
    })
    let twitterUsers = []
    let coinsInfo = []
    let value = {};
    const values = [];
    usersArr = [];
    axios.all([req1, req2]).then(axios.spread((...responses) => {
      twitterUsers = responses[0].data.data;
      coinsInfo = responses[1].data.data;
    })).then(() => {
      twitterUsers.forEach((el,index) => {
        currencyInfo = coinsInfo.filter((item) => item.slug == acount[index].coinMarket);
        const time = Date.now();
        value = {
          id:el.id,
          username: el.username,
          public_metrics: el.public_metrics,
          time:time,
          currencyInfo:currencyInfo[0].quote.USD
        }
        values.push(value);
      });
      const newModel = new CryptoModel({
        data:values
      });
      newModel.save()
    });
  });
};

module.exports = schedule;
